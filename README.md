# Persistence

I have created a simple persistence feature of the app which is favoriting movies, I'm using user defaults for this to simplify the persistence and since it is not important data.   

We can use other methods of persistence like CoreData or Realm to store data from the apis locally in the device and have an offline mode for example.
For persisting sensitive data we can use Keychain instead.

# Architecture

I have used MVVM architecture with a bit of Presenter pattern. Ofcourse the main advantage of MVVM over the usual MVC is we can separate the main business logic from the view controller and move it to the view model. We have a problem of Massive view controller wherein almost all logic is inside the view controller, this is solved by using MVVM and other related architecture. MVVM follows the single responsibility principle albeit not perfect, in my opinion, the architectural decision depends on the project and the modules involved, we need to see the bigger picture and not always stick to one architecture. For example if the module is very small, maybe applying MVVM with a mix of RXSwift might be overkill, but if it's a large enough module, then we can apply the proper architecture for it.

In the app source code you will see a usage of Singleton, Factory design patterns. And also a service layer pattern so that network requests is on a separate layer with the viewmodel and model. And a Router pattern to organize api endpoints.

# Code Documentation

I have put MARKs as I do in my practice, for me putting comments on every function is uneccessary, function names should be clear and understandable to what the code does, otherwise if it can't be fully understood just by the function name then maybe the naming is wrong, or if it really can't be broken down to a simpler function then maybe it is necessary to put comments above the function.

# Some Notes

* I have added a search function which changes the search term of the api request, swipe down to display the search bar.
* User can favorite movies in the list and in the detail view
* Added buy button to simulate buying the movie
* Detail view is a scroll view, so should be compatible with iPhone 5 size.
* Trailer can be played from the detail view
* Added rating and run time in the detail view besides the long description