//
//  AssetImageFactory.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

class AssetImageFactory {
    private init() {}
    static let shared = AssetImageFactory()

    func getAssetImage(identifier: AssetIdentifier) -> UIImage? {
        return UIImage(named: identifier.rawValue)
    }
}
