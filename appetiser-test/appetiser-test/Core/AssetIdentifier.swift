//
//  AssetConstants.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

enum AssetIdentifier: String {
    case placeholderImage = "img_placeholder"
    case favoriteIcon = "ic_favorite"
    case favoriteFilledIcon = "ic_favorite_filled"
}
