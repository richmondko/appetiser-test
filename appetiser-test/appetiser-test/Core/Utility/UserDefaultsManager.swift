//
//  UserDefaultsManager.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

class FavoriteMoviesManager {
    private init() {}
    static let shared = FavoriteMoviesManager()
    private let userDefaults = UserDefaults.standard
    private let favoriteMovies = "favoriteMovies"

    func addFavoriteMovie(with id: Double) {
        var favoritedMovieIds = userDefaults.array(forKey: favoriteMovies) as? [Double]
        favoritedMovieIds?.append(id)
        userDefaults.set(favoritedMovieIds ?? [], forKey: favoriteMovies)
    }

    func isMovieFavorited(with id: Double) -> Bool {
        guard let favoritedMovieIds = userDefaults.array(forKey: favoriteMovies) as? [Double] else { return false }
        return favoritedMovieIds.contains(id)
    }

    func unfavoriteMovie(with id: Double) {
        var favoritedMovieIds = userDefaults.array(forKey: favoriteMovies) as? [Double]
        favoritedMovieIds?.removeAll(where: { (favoriteId) -> Bool in
            return favoriteId == id
        })
        userDefaults.set(favoritedMovieIds, forKey: favoriteMovies)
    }
}
