//
//  DisplayUtil.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

class DisplayUtil {
    class func displayAlert(title: String,
                            message: String,
                            buttonDesc: String,
                            parentViewController: UIViewController,
                            completionHandler: @escaping ()-> ()) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let action = UIAlertAction(title: buttonDesc,
                                   style: .default) { (action) in
                                    alertController.dismiss(animated: true,
                                                            completion: nil)
                                    completionHandler()
        }

        alertController.addAction(action)
        parentViewController.present(alertController,
                                     animated: true,
                                     completion: nil)
    }

    class func displayConfirmation(title: String,
                                   message: String,
                                   parentViewController: UIViewController,
                                   okButtonText: String = "Okay",
                                   cancelButtonText: String = "Cancel",
                                   completionHandler: @escaping ()-> ()) {

        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let action1 = UIAlertAction(title: cancelButtonText,
                                    style: .default) { (action) in
                                        alertController.dismiss(animated: true,
                                                                completion: nil)
        }

        let action2 = UIAlertAction(title: okButtonText,
                                    style: .default) { (action) in
                                        alertController.dismiss(animated: true,
                                                                completion: nil)
                                        completionHandler()
        }

        alertController.addAction(action1)
        alertController.addAction(action2)
        parentViewController.present(alertController,
                                     animated: true,
                                     completion: nil)
    }

    class func displayOkAlert(text: String,
                              parentViewController: UIViewController) {
        DisplayUtil.displayAlert(title: "",
                                 message: text,
                                 buttonDesc: "Okay",
                                 parentViewController: parentViewController) {

        }
    }

    class func displayOkAlert(title: String, message: String,
                              parentViewController: UIViewController) {
        DisplayUtil.displayAlert(title: title,
                                 message: message,
                                 buttonDesc: "Okay",
                                 parentViewController: parentViewController) {

        }
    }

    class func displayOkAlert(title: String,
                              parentViewController: UIViewController,
                              completionHandler: @escaping ()-> ()) {
        DisplayUtil.displayAlert(title: title,
                                 message: "",
                                 buttonDesc: "Okay",
                                 parentViewController: parentViewController,
                                 completionHandler: completionHandler)

    }

    class func displayOkAlert(message: String,
                              parentViewController: UIViewController,
                              completionHandler: @escaping ()-> ()) {
        DisplayUtil.displayAlert(title: "",
                                 message: message,
                                 buttonDesc: "Okay",
                                 parentViewController: parentViewController,
                                 completionHandler: completionHandler)

    }

    class func displayOkAlert(title: String, message: String,
                              parentViewController: UIViewController,
                              completionHandler: @escaping ()-> ()) {
        DisplayUtil.displayAlert(title: title,
                                 message: message,
                                 buttonDesc: "Okay",
                                 parentViewController: parentViewController,
                                 completionHandler: completionHandler)

    }
}
