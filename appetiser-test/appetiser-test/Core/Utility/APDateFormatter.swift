//
//  APDateFormatter.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

class APDateFormatter {
    static let dateFormatter = DateFormatter()

    static func formatDateForUIReleaseDate(stringDate: String?) -> String {
        guard let stringDate = stringDate else { return "Unknown release date." }
        APDateFormatter.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        if let date = APDateFormatter.dateFormatter.date(from: stringDate) {
            APDateFormatter.dateFormatter.dateFormat = "dd MMM yyyy"
            return dateFormatter.string(from: date)
        } else {
            return "Unknown release date."
        }
    }
}
