//
//  Router.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {

    case getMovies(searchTerm: String)

    static let baseURLString = "https://itunes.apple.com"

    private var method: HTTPMethod {
        switch self {
        case .getMovies: return .get
        }
    }

    private var path: String {
        switch self {
        case .getMovies(_):
            return "/search"
        }
    }


    // MARK: - URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseURLString.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")

        switch self {
        case .getMovies(let searchTerm):
            let parameters: [String: Any] = [
                "term": searchTerm,
                "country": "au",
                "media": "movie"
            ]
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }

}
