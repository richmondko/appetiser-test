//
//  Coordinator.swift
//  appetiser-test
//
//  Created by Richmond Ko on 03/09/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}
