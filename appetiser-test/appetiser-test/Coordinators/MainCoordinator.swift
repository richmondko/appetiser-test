//
//  MainCoordinator.swift
//  appetiser-test
//
//  Created by Richmond Ko on 03/09/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = MovieListViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }

    func showMovieDetail(viewModel: MovieViewModel) {
        let vc = MovieDetailViewController.instantiate()
        vc.coordinator = self
        vc.movieViewModel = viewModel
        navigationController.pushViewController(vc, animated: true)
    }
}
