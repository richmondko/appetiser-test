//
//  MovieService.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class MovieService {
    private init() {}
    static let shared = MovieService()

    func getMovies(searchTerm: String, withBlock completion: @escaping (MovieResponse?, Error?) -> Void) {
        Alamofire.request(Router.getMovies(searchTerm: searchTerm)).responseDecodableObject(decoder: JSONDecoder()) { (response: DataResponse<MovieResponse>) in
            switch response.result {
            case .success(let movieResponse):
                completion(movieResponse, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
