//
//  MovieTableViewCell.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit
import Kingfisher

protocol MovieTableViewCellDelegate: class {
    func didTapBuyButton(with movieTitle: String)
}

class MovieTableViewCell: UITableViewCell {

    // MARK: - Stored (IBOutlet)
    private var viewModel: MovieViewModel!
    weak var delegate: MovieTableViewCellDelegate?

    // MARK: - Stored (IBOutlet)
    @IBOutlet var artworkImageView: UIImageView!
    @IBOutlet var movieTitleLabel: UILabel!
    @IBOutlet var genreLabel: UILabel!
    @IBOutlet var priceButton: UIButton!
    @IBOutlet var releaseDateLabel: UILabel!
    @IBOutlet var favoriteButton: UIButton!
    
    // MARK: - App View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteButton.tintColor = UIColor.red
    }

    func configure(with movieViewModel: MovieViewModel) {
        self.viewModel = movieViewModel

        let placeholder = AssetImageFactory.shared.getAssetImage(identifier: AssetIdentifier.placeholderImage)
        if let url = movieViewModel.imageURL {
            artworkImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.2))])
        } else {
            artworkImageView.image = placeholder
        }

        movieTitleLabel.text = movieViewModel.movieName
        genreLabel.text = movieViewModel.genre
        priceButton.setTitle(movieViewModel.price, for: .normal)
        releaseDateLabel.text = movieViewModel.releaseDate
        favoriteButton.setImage(movieViewModel.isFavoriteImage, for: .normal)
    }

    @IBAction func didTapFavoriteButton(_ sender: Any) {
        viewModel.toggleFavorite()
        favoriteButton.setImage(viewModel.isFavoriteImage, for: .normal)
    }

    @IBAction func didTapBuyButton(_ sender: Any) {
        delegate?.didTapBuyButton(with: viewModel.movieName ?? "")
    }
}

