//
//  MovieDetailViewController.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit
import AVKit

protocol MovieDetailViewControllerDelegate: class {
    func didTapFavoriteButton()
}

class MovieDetailViewController: UIViewController, Storyboarded {

    // MARK: - Stored
    var movieViewModel: MovieViewModel?
    weak var delegate: MovieDetailViewControllerDelegate?
    weak var coordinator: MainCoordinator?
    
    // MARK: - Stored (IBOutlet)
    @IBOutlet var artworkImageView: UIImageView!
    @IBOutlet var movieTitleLabel: UILabel!
    @IBOutlet var movieGenreLabel: UILabel!
    @IBOutlet var releaseDateLabel: UILabel!
    @IBOutlet var buyButton: UIButton!
    @IBOutlet var rentButton: UIButton!
    @IBOutlet var movieDescription: UILabel!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var movieRatingLabel: UILabel!
    @IBOutlet var runtimeLabel: UILabel!
    @IBOutlet var favoriteButton: UIButton!

    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewModelToUI()
    }

    // MARK: - Instance

    private func setViewModelToUI() {
        guard let movieViewModel = movieViewModel else { return }

        let placeholder = AssetImageFactory.shared.getAssetImage(identifier: AssetIdentifier.placeholderImage)
        if let url = movieViewModel.imageURL {
            artworkImageView.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.2))])
        } else {
            artworkImageView.image = placeholder
        }

        movieTitleLabel.text = movieViewModel.movieName
        movieGenreLabel.text = movieViewModel.genre
        releaseDateLabel.text = movieViewModel.releaseDate
        buyButton.setTitle(movieViewModel.price, for: .normal)
        rentButton.setTitle(movieViewModel.rentalPrice, for: .normal)
        movieDescription.text = movieViewModel.description
        movieRatingLabel.text = movieViewModel.rating
        runtimeLabel.text = movieViewModel.runTime
        favoriteButton.setImage(movieViewModel.isFavoriteImage, for: .normal)
        favoriteButton.tintColor = UIColor.red
    }

    private func playTrailer() {
        guard let trailerURL = movieViewModel?.trailer else { return }

        let playerController = AVPlayerViewController()
        let player = AVPlayer(url: trailerURL)
        playerController.player = player
        player.play()
        present(playerController, animated: true, completion: nil)
    }

    // MARK: - Instance (IBAction)
    @IBAction func didTapPlayButton(_ sender: Any) {
        playTrailer()
    }

    @IBAction func didTapFavoriteButton(_ sender: Any) {
        guard let movieViewModel = movieViewModel else { return }
        movieViewModel.toggleFavorite()
        favoriteButton.setImage(movieViewModel.isFavoriteImage, for: .normal)
        delegate?.didTapFavoriteButton()
    }

    @IBAction func didTapRentButton(_ sender: Any) {
        DisplayUtil.displayAlert(title: "Rent", message: "Are you sure you want to rent \(movieViewModel?.movieName ?? "")?", buttonDesc: "Yes!", parentViewController: self) {
            DisplayUtil.displayOkAlert(text: "Just kidding, this is not real.", parentViewController: self)
        }
    }

    @IBAction func didTapBuyButton(_ sender: Any) {
        DisplayUtil.displayAlert(title: "Purchase", message: "Are you sure you want to buy \(movieViewModel?.movieName ?? "")?", buttonDesc: "Yes!", parentViewController: self) {
            DisplayUtil.displayOkAlert(text: "Just kidding, this is not real.", parentViewController: self)
        }
    }
}
