//
//  MovieListViewController.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController, Storyboarded {

    // MARK: - Stored
    private let movieCellIdentifier = "movieCell"
    private let moviesViewModelPresenter = MoviesViewModelPresenter()
    private var refreshControl = UIRefreshControl()
    private let searchController = UISearchController(searchResultsController: nil)
    weak var coordinator: MainCoordinator?
    
    // MARK: - Stored (IBOutlet)
    @IBOutlet var tableView: UITableView!

    // MARK: - App View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRefreshControl()
        configureTableView()
        configureMoviesViewModelPresenter()
        configureSearchController()
        getMovies(searchTerm: "star")
    }

    // MARK: - Instance
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func configureMoviesViewModelPresenter() {
        moviesViewModelPresenter.delegate = self
    }

    private func configureRefreshControl() {
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tableView.addSubview(refreshControl)
    }

    private func configureSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Movies"
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        getMovies(searchTerm: "star")
    }

    private func getMovies(searchTerm: String) {
        moviesViewModelPresenter.getMovies(searchTerm: searchTerm)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesViewModelPresenter.movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: movieCellIdentifier, for: indexPath) as! MovieTableViewCell
        cell.configure(with: moviesViewModelPresenter.movies[indexPath.row])
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = moviesViewModelPresenter.movies[indexPath.row]
        coordinator?.showMovieDetail(viewModel: viewModel)
    }
}

// MARK: - MoviesViewModelPresenterDelegate
extension MovieListViewController: MoviesViewModelPresenterDelegate {
    func didReceiveMovies() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }

    func didStartFetchingData() {
    }

    func didReceiveUnknownError() {
        refreshControl.endRefreshing()
        DisplayUtil.displayOkAlert(title: "Error", message: "Something went wrong, please try again later or report this to us.", parentViewController: self)
    }

    func didReceiveError(error: Error) {
        refreshControl.endRefreshing()
        DisplayUtil.displayOkAlert(title: "Error", message: error.localizedDescription, parentViewController: self)    }
}

// MARK: - UISearchResultsUpdating
extension MovieListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        getMovies(searchTerm: searchController.searchBar.text ?? "")
    }
}

// MARK: - UISearchBarDelegate
extension MovieListViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        getMovies(searchTerm: "star")
    }
}

// MARK: - MovieDetailViewControllerDelegate
extension MovieListViewController: MovieDetailViewControllerDelegate {
    func didTapFavoriteButton() {
        tableView.reloadData()
    }
}

// MARK: - MovieTableViewCellDelegate
extension MovieListViewController: MovieTableViewCellDelegate {
    func didTapBuyButton(with movieTitle: String) {
        DisplayUtil.displayAlert(title: "Purchase", message: "Are you sure you want to buy \(movieTitle)?", buttonDesc: "Yes!", parentViewController: self) {
            DisplayUtil.displayOkAlert(text: "Just kidding, this is not real.", parentViewController: self)
        }
    }
}
