//
//  MoviesViewModel.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

protocol MoviesViewModelPresenterDelegate: class {
    func didReceiveMovies()
    func didStartFetchingData()
    func didReceiveError(error: Error)
    func didReceiveUnknownError()
}

class MoviesViewModelPresenter {
    weak var delegate: MoviesViewModelPresenterDelegate?
    private let movieService = MovieService.shared
    var movies: [MovieViewModel] = []

    func getMovies(searchTerm: String) {
        delegate?.didStartFetchingData()
        movieService.getMovies(searchTerm: searchTerm) { [weak self] (moviesResponse, error) in
            guard let self = self else { return }
            if let movies = moviesResponse?.results {
                self.movies.removeAll()
                let movieViewModels = movies.map({ (movie) -> MovieViewModel in
                    return MovieViewModel(movieData: movie)
                })
                self.movies.append(contentsOf: movieViewModels)
                self.delegate?.didReceiveMovies()
            } else if let error = error {
                self.delegate?.didReceiveError(error: error)
            } else {
                self.delegate?.didReceiveUnknownError()
            }
        }
    }
}
