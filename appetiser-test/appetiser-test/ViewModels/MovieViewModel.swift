//
//  MovieViewModel.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation
import UIKit

class MovieViewModel {
    let id: Double!
    let imageURL: URL?
    let movieName: String?
    let genre: String?
    let releaseDate: String?
    let price: String?
    let description: String?
    let artistName: String?
    let rating: String?
    let rentalPrice: String?
    let trailer: URL?
    let runTime: String?
    private var isFavorite: Bool = false
    var isFavoriteImage: UIImage?

    init(movieData: Movie) {
        self.id = movieData.trackId
        self.imageURL = movieData.artworkUrl100
        self.movieName = movieData.trackName
        self.genre = movieData.primaryGenreName
        self.releaseDate = APDateFormatter.formatDateForUIReleaseDate(stringDate: movieData.releaseDate)
        self.price = "\(movieData.currency ?? "") \(movieData.trackPrice ?? 0) BUY"
        self.description = movieData.longDescription
        self.artistName = movieData.artistName
        self.rating = movieData.contentAdvisoryRating
        self.rentalPrice = "\(movieData.currency ?? "") \(movieData.trackRentalPrice ?? 0) RENT"
        self.trailer = movieData.previewUrl
        self.runTime = movieData.trackTimeMillis?.stringFromTimeInterval()

        if let id = movieData.trackId {
            isFavorite = FavoriteMoviesManager.shared.isMovieFavorited(with: id)
            if FavoriteMoviesManager.shared.isMovieFavorited(with: id) {
                isFavoriteImage = AssetImageFactory.shared.getAssetImage(identifier: .favoriteFilledIcon)?.withRenderingMode(.alwaysTemplate)
            } else {
                isFavoriteImage = AssetImageFactory.shared.getAssetImage(identifier: .favoriteIcon)?.withRenderingMode(.alwaysTemplate)
            }
        } else {
            isFavoriteImage = AssetImageFactory.shared.getAssetImage(identifier: .favoriteIcon)?.withRenderingMode(.alwaysTemplate)
        }
    }

    func toggleFavorite() {
        if isFavorite {
            isFavorite = false
            FavoriteMoviesManager.shared.unfavoriteMovie(with: self.id)
            isFavoriteImage = AssetImageFactory.shared.getAssetImage(identifier: .favoriteIcon)?.withRenderingMode(.alwaysTemplate)
        } else {
            isFavorite = true
            FavoriteMoviesManager.shared.addFavoriteMovie(with: self.id)
            isFavoriteImage = AssetImageFactory.shared.getAssetImage(identifier: .favoriteFilledIcon)?.withRenderingMode(.alwaysTemplate)
        }
    }

    
}
