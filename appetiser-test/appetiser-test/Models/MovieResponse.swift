//
//  MovieResponse.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

struct MovieResponse: Codable {
    let resultCount: Int?
    let results: [Movie]?
}
