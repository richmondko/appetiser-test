//
//  Movie.swift
//  appetiser-test
//
//  Created by Richmond Ko on 20/08/2019.
//  Copyright © 2019 Richmond Ko. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let trackId: Double?
    let artistName: String?
    let collectionName: String?
    let trackName: String?
    let artworkUrl30: URL?
    let artworkUrl60: URL?
    let artworkUrl100: URL?
    let trackPrice: Double?
    let trackTimeMillis: Double?
    let releaseDate: String?
    let country: String?
    let currency: String?
    let primaryGenreName: String?
    let contentAdvisoryRating: String?
    let shortDescription: String?
    let longDescription: String?
    let previewUrl: URL?
    let trackRentalPrice: Double?

    enum CodingKeys: String, CodingKey {
        case trackId
        case artistName
        case collectionName
        case trackName
        case artworkUrl30
        case artworkUrl60
        case artworkUrl100
        case trackPrice
        case trackTimeMillis
        case releaseDate
        case country
        case currency
        case primaryGenreName
        case contentAdvisoryRating
        case shortDescription
        case longDescription
        case previewUrl
        case trackRentalPrice
    }
}
